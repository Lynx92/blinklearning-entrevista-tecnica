// 2.- Implementar un componente React que muestre una lista desordenada
//  dado un array de strings. Por ejemplo, el resultado del array
//  [“Amarillo”, “Azul”, “Rojo”, “Verde”] deberá ser algo como :

// 	<ul>
// 		<li>Amarillo</li>
// 		<li>Azul</li>
// 		<li>Rojo</li>
// 		<li>Verde</li>
// 	</ul>

import React, { Component } from "../../../node_modules/react";

export default class List extends Component {
  constructor() {
    super();
    this.state = {
      colorsArr: ["Amarillo", "Azul", "Rojo", "Verde"]
    };
  }
  render() {
    return (
      <div>
        <hr />
        <ul>
          {this.state.colorsArr.map(color => (
            <li key={color}>{color}</li>
          ))}
        </ul>
        <hr />
      </div>
    );
  }
}
