// JavaScript:
// Implementar la función sumaArray para calcular,
//  de forma genérica, la suma de los elementos de un array numérico
//  (incluyendo varios niveles).

// 	function sumaArray(lista) {…}

// Ejemplos de arrays a pasar en la función:

var lista1 = [1, 4, 3, 0]; // Expected => 8
var lista2 = [1, 2, [5, 8], 4]; // Expected => 20
var lista3 = [0, [6, 2], null, 7, 1]; // Expected => 16


function sumaArray(lista) {
  let newList = lista.flat(); // Dejar el array en 1 solo nivel
  let result = newList.reduce((a, b) => a + b); // sumar todos los elementos de ese array
  return result; // devolvemos el resultado de la suma de sus elementos
}


// sumaArray(lista1);
// sumaArray(lista2);
sumaArray(lista3);
