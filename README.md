Nombre: Gabriel Belmonte Moreno
Tiempo invertido en realizar la prueba: 6 horas


JavaScript:
Implementar la función sumaArray para calcular, de forma genérica, la suma de los elementos de un array numérico (incluyendo varios niveles). 

	function sumaArray(lista) {…}

Ejemplos de arrays a pasar en la función:

	var lista1 = [1, 4, 3, 0];
	var lista2 = [1, 2, [5, 8], 4];
	var lista3 = [0, [6, 2], null, 7, 1];

React.js
1.- ¿Qué pasa cuando sea llama a setState?
2.- Implementar un componente React que muestre una lista desordenada dado un array de strings. Por ejemplo, el resultado del array [“Amarillo”, “Azul”, “Rojo”, “Verde”] deberá ser algo como :

	<ul>
		<li>Amarillo</li>
		<li>Azul</li>
		<li>Rojo</li>
		<li>Verde</li>
	</ul>

!!!!!(IMPORTANTE HACER "npm i" ANTES DE PROBARLO)!!!!!

CSS: 
Crear este botón:
Btn.png


PARTE BACK (PHP)
1. Implementar la función sumaArray para calcular la suma de los elementos de un array numérico:
function sumaArray($ar) {}
Ejemplos de arrays a pasar en la función:
	var lista1 = [1, 4, 3, 0];
	var lista2 = [1, 2, [5, 8], 4];
	var lista3 = [0, [6, 2], null, 7, 1];


2. ¿Cuál es la salida de este código: $i=0;while ($i=1) echo ++$i;?

3. ¿Qué función en PHP utilizarías para quitar todos los espacios de un string?
