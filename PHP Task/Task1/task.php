<?php
// 1. Implementar la función sumaArray para calcular la suma de los elementos de un array
//  numérico:
// function sumaArray($ar) {}
// Ejemplos de arrays a pasar en la función:
// 	var lista1 = [1, 4, 3, 0];
// 	var lista2 = [1, 2, [5, 8], 4];
// 	var lista3 = [0, [6, 2], null, 7, 1];

$lista1 = [1, 4, 3, 0]; // Expected => 8
$lista2 = [1, 2, [5, 8], 4]; // Expected => 20
$lista3 = [0, [6, 2], null, 7, 1]; // Expected => 16


function sumaArray($ar)
{
    $flattenArr = [];
    array_walk_recursive($ar, function ($onlyOneLevel) use (&$flattenArr) {
        $flattenArr[] = $onlyOneLevel; // Reducimos el array a un solo nivel
    });
    $result = array_sum($flattenArr); // Sumamos elementos del array
    echo $result; // Mostramos el resultado de la suma 
}

// sumaArray($lista1);
// sumaArray($lista2);
sumaArray($lista3);
